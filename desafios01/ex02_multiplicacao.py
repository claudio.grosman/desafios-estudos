def multiplicacao_recursiva(valor1, valor2):
    if valor2 == 0 or valor1 == 0:
        return 0
    elif valor1 == 1:
        return valor2
    elif valor2 < 0:
        return - multiplicacao_recursiva(valor1, -valor2)
    else:
        return valor1 + multiplicacao_recursiva(valor1, valor2-1)

print(multiplicacao_recursiva(-4,3))
