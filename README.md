# Desafios Estudos

Projeto para armazenar todos os exercícios exclusivamente para Estudos

Ex01: Desenvolver um game blackjack 21
        - Desenvolver front
        - Automatizar a aplicação

Ex02: Criar uma função de multiplicação sem o operador * e sem loop

Ex03: Criar uma função de soma sem usar o operador de soma

Ex04: Desenvolver uma aplicação web CRUD com Flask e sqlite
        - Efetuar testes TDD na aplicação criada
        - Automatizar a aplicação com Selenium
        - Desenvolver um teste BDD

Ex05: Desenvolver jogo da velha usando React:
        - https://pt-br.reactjs.org/tutorial/tutorial.html
        - Alterar para que o jogador consiga entrar com seu nome e aparecer o nome do ganhador
